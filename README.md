# Aurora
A minimal AUR-helper written in two lines of Python!

### Usage

```
python3 main.py <packagename>
```

### Tips

I recommend adding an alias to your `.bashrc` file for Aurora like this

```
alias 'aurora'='python3 /path/to/aurora/main.py'
```

so you can have easy access to Aurora in your command line

```
aurora <packagename>
```

### Make/Install
you can make the project using
```
make main
``` 
it will output the binary to build/aurora and then you can move it into /usr/bin to use it or use
```
make install
``` 
which will make and install it for you
