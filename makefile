main:
	pyinstaller --clean --specpath build/specs -n aurora -F main.py
	
clean:
	rm -r build/aurora
	rm -r src/__pycache__
	
install:
	pyinstaller --clean --specpath build/specs -n aurora -F main.py
	sudo cp build/aurora /usr/bin
